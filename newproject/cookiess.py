from flask import *  
      
app = Flask(__name__)  

     
@app.route('/error')  
def error():  
    return "<p><strong>Enter correct password</strong></p>"  
     


@app.route('/')  
def home():  
    return render_template("home.html")  




@app.route('/login')  
def login():  
    return render_template("login.html")  
     
@app.route('/success',methods = ['POST'])  
def success():  
    if request.method == "POST":  
        email = request.form['email']  
        password = request.form['pass']
        f = request.files['file']  
        f.save(f.filename)
        resp = make_response(render_template('success.html',  name = f.filename))  
        resp.set_cookie('email',email)
        resp.set_cookie('pass',password)
        return resp 

          
    # if password =="password":  
    #     resp = make_response(render_template('success.html'))  
    #     resp.set_cookie('email',email)
    #     return resp  
    else:  
        return redirect(url_for('error'))  





     
@app.route('/viewprofile')  
def profile():  
    email = request.cookies.get('email')  
    resp = make_response(render_template('profile.html',name = email))  
    return resp  


@app.route('/result',methods = ['POST', 'GET'])
def result():
   if request.method == 'POST':
      result = request.form
      return render_template("result.html",result = result)
      
if __name__ == "__main__":  
    app.run(debug = True)  