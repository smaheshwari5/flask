from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def student():
   return render_template('student.html')

@app.route('/result',methods = ['POST', 'GET'])
def result():
   if request.method == 'POST':
      result = request.form
      return render_template("result.html",result = result)


 
# @app.route('/cookie')  
# def cookie():  
#     # res = make_response("<h1>cookie is set</h1>")  
#     # res.set_cookie('foo','bar')  
#     # return res 
#     return  "Hello Flask Cookeis !!!"

if __name__ == '__main__':
   app.run(debug = True)