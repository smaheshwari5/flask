from flask import *  
app = Flask(__name__)  
app.secret_key = "sanjana"  
     
@app.route('/')  
def home():  
    return render_template("home2.html")  
     
@app.route('/login')  
def login():  
    return render_template("login2.html")  
     
@app.route('/success',methods = ["POST"])  
def success():  
    if request.method == "POST":  
        session['email']=request.form['email']  
    return render_template('success1.html')  
     
@app.route('/logout')  
def logout():  
    if 'email' in session:  
        session.pop('email',None)  
        return render_template('logout.html');  
    else:  
        return '<p>user already logged out</p>'  
     
@app.route('/profile')  
def profile():  
    if 'email' in session:  
        email = session['email']  
        return render_template('profile.html',name=email)  
    else:  
        return '<p>Please login first</p>' 

@app.route('/result',methods = ['POST', 'GET'])
def result():
   if request.method == 'POST':
      result = request.form
      return render_template("result.html",result = result) 
      
if __name__ == '__main__':  
    app.run(debug = True)  